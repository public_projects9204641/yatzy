import java.util.Arrays;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Yatzy {

    protected int[] dice;

    public Yatzy(int d1, int d2, int d3, int d4, int d5)
    {
        dice = new int[5];
        dice[0] = d1;
        dice[1] = d2;
        dice[2] = d3;
        dice[3] = d4;
        dice[4] = d5;
    }
    public static int chance(int... dice) {
        return Arrays.stream(dice).sum();
    }

    public static int yatzy(int... dice)
    {
        int[] counts = new int[6];
        for (int die : dice)
            counts[die-1]++;
        for (int i = 0; i != 6; i++)
            if (counts[i] == 5)
                return 50;
        return 0;
    }

    public static int sumDice(int e, int... dice) {
        return Arrays.stream(dice).filter(d -> d == e).sum();
    }

    public static int ones(int... dice) {
        return sumDice(1, dice);
    }

    public static int twos(int... dice) {
        return sumDice(2, dice);
    }

    public static int threes(int... dice) {
        return sumDice(3, dice);
    }

    public int fours() {
        return sumDice(4, dice);
    }

    public int fives() {
        return sumDice(5, dice);
    }

    public int sixes() {
        return sumDice(6, dice);
    }

    public static int score_pair(int d1, int d2, int d3, int d4, int d5) {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);
        for (int i = 0; i != 6; i++)
            if (tallies[6-i-1] >= 2)
                return (6-i)*2;
        return 0;
    }

    public static int two_pair(int d1, int d2, int d3, int d4, int d5)
    {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);
        int n = 0;
        int score = 0;
        for (int i = 0; i < 6; i += 1)
            if (tallies[6-i-1] >= 2) {
                n++;
                score += (6-i);
            }
        return score == 0 ? 0 : score * 2;
    }

    public static int four_of_a_kind(int d1, int d2, int d3, int d4, int d5)
    {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);
        for (int i = 0; i < 6; i++)
            if (tallies[i] >= 4)
                return (i+1) * 4;
        return 0;
    }

    public static int three_of_a_kind(int d1, int d2, int d3, int d4, int d5)
    {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);
        for (int i = 0; i < 6; i++)
            if (tallies[i] >= 3)
                return (i+1) * 3;
        return 0;
    }

    public static int smallStraight(int d1, int d2, int d3, int d4, int d5) {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);

        return Arrays.stream(tallies).limit(5).allMatch(e -> e == 1) ? 15 : 0;
    }

    public static int largeStraight(int d1, int d2, int d3, int d4, int d5) {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);
        if (tallies[1] == 1 &&
            tallies[2] == 1 &&
            tallies[3] == 1 &&
            tallies[4] == 1 &&
            tallies[5] == 1)
            return 20;
        return 0;
    }

    public static int fullHouse(int d1, int d2, int d3, int d4, int d5) {
        int[] tallies = initTallies(d1, d2, d3, d4, d5);

        OptionalInt twoSame = IntStream.range(0, 6)
            .filter(i -> tallies[i] == 2)
            .map(i -> (i + 1) * 2)
            .findFirst();

        OptionalInt threeSame = IntStream.range(0, 6)
            .filter(i -> tallies[i] == 3)
            .map(i -> (i + 1) * 3)
            .findFirst();

        if (twoSame.isPresent() && threeSame.isPresent()) {
            return twoSame.getAsInt() + threeSame.getAsInt();
        } else {
            return 0;
        }
    }

    private static int[] initTallies(int... dice) {
        int[] tallies = new int[6];
        Arrays.stream(dice).forEach(d -> tallies[d-1]++);
        return tallies;

    }
}



